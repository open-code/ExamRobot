package com.exrebound.examrobot;

/**
 * Created by hp sleek book on 4/21/2015.
 */
public class MenuItem {
    String itemName;
    int drawableID;

    public MenuItem(String itemName, int drawableID) {
        this.itemName = itemName;
        this.drawableID = drawableID;
    }

    public String getItemName() {
        return itemName;
    }


    public int getDrawableID() {
        return drawableID;
    }

}
