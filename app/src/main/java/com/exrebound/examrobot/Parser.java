package com.exrebound.examrobot;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Zuhaib on 3/21/2015.
 */
public class Parser {

    public static List<Card> parseCards(String response){
        try{
            JSONArray array = new JSONArray(response);
            List<Card> cards = new ArrayList<>();

            for(int i = 0 ; i < array.length() ; i++){
                JSONObject object = array.getJSONObject(i);
                Card c = new Card();

                c.setID(object.getInt("ID"));
                c.setQuestion(object.getString("question"));
                c.setAnswer(object.getString("answer"));
                c.setTag(object.getString("tag"));
                c.setTimestamp(object.getString("timestamp"));

                cards.add(c);
            }

            return cards;
        }catch(JSONException e){
            e.printStackTrace();
            return null;
        }
    }
}
