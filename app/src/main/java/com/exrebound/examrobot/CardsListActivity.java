package com.exrebound.examrobot;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpStatus;
import org.json.JSONException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CardsListActivity extends Fragment{

    public static final String EXTRA_FILENAME = "cards";
    public static final String EXTRA_TAG_ARRAY = "tag_array";
    public static final String EXTRA_DATE_ARRAY = "date_array";
    public static final String EXTRA_IMPORTANT_ARRAY = "imp_array";
    public static final String EXTRA_INVALID_CREDENTIALS = "invalid_credentials";

    public static final String EXTRA_COUNTER = "counter";

    //protected static String FC_URL = "http://examrobot.com/api/blog/blog/retrieve?nitems=0&since=10";
    protected static String FC_URL = "http://examrobot.com/api/galaxyionew/galaxyionew/retrieve?nitems=20&since=10";
    protected static final String EXTRA_QUESTION = "question";
    protected static final String EXTRA_ANSWER = "answer";
    protected static final String EXTRA_TAG = "tag";
    protected static final String EXTRA_TIME = "time";
    protected static final String EXTRA_IMPORTANT = "important";
    protected static final String EXTRA_POSITION = "position";
    protected static final String TAG = "CardsListActivity";
    protected static final String EXTRA_ID = "id";
    protected static final int REQUEST_CODE = 0;

    ListView cardsList;
    ProgressBar p;

    String username,password;
    boolean imp;
    Calendar date;
    private int year;
    private int month;
    private int day;
    boolean isFirstTime;

    ArrayList<Card> cards;
    ArrayAdapter adapter;

    JSONSerializer mSerializer;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(getActivity(), "DEFAULT");

        mSerializer = new JSONSerializer(getActivity(),"Cards.json");

        username = getArguments().getString(FlashCardPagerActivity.EXTRA_USERNAME);
        password = getArguments().getString(FlashCardPagerActivity.EXTRA_PASSWORD);

        Log.d(TAG, "Username: " + username + " Password: " + password);

        //Get reference to shared prefs
        prefs = getActivity().getSharedPreferences("LoginPrefs", Context.MODE_PRIVATE);

        if(prefs.contains(EXTRA_COUNTER)){
            Log.d(TAG,"Prefs Found!");
            Log.d(TAG,"Count: "+prefs.getInt(EXTRA_COUNTER,0));
        }else if(isFirstTime){
            Log.d(TAG,"Prefs Not Available!");
        }else{
            Log.d(TAG,"Prefs Not Found!");
        }
        editor = prefs.edit();

        if(username == null && password == null){
            username = prefs.getString(LoginActivity.EXTRA_USERNAME,"");
            password = prefs.getString(LoginActivity.EXTRA_PASSWORD,"");
        }

        isFirstTime = getArguments().getBoolean(FlashCardPagerActivity.EXTRA_CALLER);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.activity_cards_list,container,false);
        p = (ProgressBar) v.findViewById(R.id.progressBar);
        p.setVisibility(View.GONE);

        cards = new ArrayList<>();

        cardsList = (ListView) v.findViewById(R.id.cards_list);
        cardsList.setVisibility(View.GONE);
        cardsList.setEmptyView(v.findViewById(R.id.emptyView));
        cardsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.d(TAG,"Cards right now: "+cards.size());

                Card c = (Card) cardsList.getItemAtPosition(position);
                Intent i = new Intent(getActivity(), FlashCardActivity.class);
                i.putExtra(EXTRA_FILENAME, "Cards.json");
                i.putExtra(EXTRA_QUESTION, c.getQuestion());
                i.putExtra(EXTRA_ANSWER, c.getAnswer());
                i.putExtra(EXTRA_TAG, c.getTag());
                i.putExtra(EXTRA_TIME, c.getTimestamp());
                i.putExtra(EXTRA_IMPORTANT, c.isImportant());
                i.putExtra(EXTRA_ID, c.getID());
                i.putExtra(EXTRA_POSITION, position);
                startActivity(i);
            }
        });

        CheckAndRequestData();

        return v;
    }

    private void CheckAndRequestData() {
        //If connected, fetch data and store in file
        if(isNetworkOnline() && isFirstTime){
            try {
                cardsList.setVisibility(View.GONE);
                p.setVisibility(View.VISIBLE);
                cards.clear();
                cards.addAll(mSerializer.loadData());
                int count = prefs.getInt(EXTRA_COUNTER,0);
                Log.d(TAG,"Count: "+count);
                if(cards != null && cards.size() > 0){
                    int ID;
                    if(count != 0)
                        ID = cards.get(count-1).getID();
                    else
                        ID = cards.get(count).getID();
                    FC_URL = "http://examrobot.com/api/galaxyionew/galaxyionew/retrieve?nitems="+ID+"&since=10";
                }
                requestData(FC_URL);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //Else fetch from file
        else{
            try {
                if(isFirstTime) {
                    Toast.makeText(getActivity(), "Network Not Found!\nWorking in Offline Mode.", Toast.LENGTH_LONG).show();
                }
                    cards.clear();
                    cards.addAll(mSerializer.loadData());
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Error Occurred While Loading Data.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
        }
        adapter = new CardsListAdapter(getActivity(), R.layout.card_item, cards);
        cardsList.setAdapter(adapter);
    }

    private void requestData(String url) {
        String credentials = username+":"+password;
        final String credBase64 = Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);

        StringRequest request = new StringRequest(Request.Method.GET,url,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            cards.addAll(0,Parser.parseCards(s));
                            int count = Parser.parseCards(s).size();
                            if(count > 0) {
                                Toast.makeText(getActivity(), count + " New Questions Added!", Toast.LENGTH_SHORT).show();
                                editor.putInt(EXTRA_COUNTER,count);
                                editor.commit();
                            }
                            else
                                Toast.makeText(getActivity(), "No New Questions Added!",Toast.LENGTH_SHORT).show();
                            p.setVisibility(View.GONE);
                            cardsList.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                            mSerializer.saveData(cards);
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), "Error Occurred While Saving Data To Local Storage.", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        p.setVisibility(View.GONE);
                        NetworkResponse networkResponse = volleyError.networkResponse;
                        if(networkResponse != null && networkResponse.statusCode == HttpStatus.SC_FORBIDDEN){
                            cards.clear();
                            adapter.notifyDataSetChanged();
                            Toast.makeText(getActivity(),"Login Unsuccessful due to Invalid Credentials.",Toast.LENGTH_LONG).show();
                            Intent i = new Intent(getActivity(),LoginActivity.class);
                            i.putExtra(EXTRA_INVALID_CREDENTIALS,true);
                            //Remove activity from stack
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }else {
                            Toast.makeText(getActivity(), "Error Occurred While Fetching Data From Server", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Authorization", "Basic "+credBase64);
                return params;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        queue.add(request);
    }

    public boolean isNetworkOnline() {
        boolean status = false;
        try{
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.refresh) {
            refresh();
            return true;
        }
        if (id == R.id.filterByTag) {
            FilterByTag();
            return true;
        }
        if(id == R.id.filterByImportant){
            FilterByImportant();
            return true;
        }
        if (id == R.id.filterByDate) {
            FilterByDateWithDialog();
            return true;
        }if(id == R.id.exit){
            //this.finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void FilterByDateWithDialog() {
        date = Calendar.getInstance();
        year = date.get(Calendar.YEAR);
        month = date.get(Calendar.MONTH);
        day = date.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog d = new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                date.set(year, monthOfYear, dayOfMonth);
                FilterByDate(date.getTime());
            }
        },year,month+1,day);
        d.show();
    }

    public void refresh() {
        if(isNetworkOnline()){
            //Animation rotate = AnimationUtils.loadAnimation(this,R.anim.rotate);
            //findViewById(R.id.refresh).startAnimation(rotate);
            isFirstTime = true;
            CheckAndRequestData();
            isFirstTime = false;
        } else
            Toast.makeText(getActivity(), "No Internet Connection! Sync Can't Be Done.", Toast.LENGTH_LONG).show();
    }

    public void FilterByTag() {
        ArrayList<String> tags = new ArrayList<>();
        for(Card c : cards){
            //If tag is not already in tag list
            if(!c.getTag().isEmpty() && !tags.contains(c.getTag())) {
                //Tag
                tags.add(c.getTag());
            }
        }

        Intent i = new Intent(getActivity(),FilterByTagsActivity.class);
        i.putStringArrayListExtra(EXTRA_TAG_ARRAY, tags);
        startActivity(i);

    }

    private void FilterByDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String dateString = df.format(date);
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ArrayList<Card> dateFilterCards = new ArrayList<>();
        for(Card c : cards) {
            if (date != null && c.getDate() != null && date.compareTo(c.getDate()) == 0) {
                dateFilterCards.add(c);
            }
        }

        Intent i = new Intent(getActivity(),FilterByDateActivity.class);
        i.putExtra(EXTRA_DATE_ARRAY, dateFilterCards);
        startActivity(i);

    }

    public void FilterByImportant() {
        ArrayList<Card> importantFilterCards = new ArrayList<>();
        for(Card c : cards) {
            if (c.isImportant())
                importantFilterCards.add(c);
        }

        Intent i = new Intent(getActivity(),FilterByImportantActivity.class);
        i.putExtra(EXTRA_IMPORTANT_ARRAY, importantFilterCards);
        startActivity(i);

    }

    //Adapter for Cards//
    private class CardsListAdapter extends ArrayAdapter<Card>{
        public CardsListAdapter(Context context, int resource, ArrayList<Card> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.card_item, null);

            final Card c = getItem(position);
            //Log.e(TAG,"Timestamp "+c.getTimestamp());
            TextView titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            titleTV.setText(c.getQuestion());
            TextView answerTV = (TextView) convertView.findViewById(R.id.answerTV);
            answerTV.setText(c.getAnswer());
            TextView tagTV = (TextView) convertView.findViewById(R.id.tagTV);
            tagTV.setText(c.getTag());
            TextView timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            if(!c.getTimestamp().equals("null")) {
                //Convert date
                long stamp = Long.valueOf(c.getTimestamp());
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(stamp * 1000L);

                Date time = calendar.getTime();

                DateFormat df = new SimpleDateFormat("dd MMM yyyy");
                String date = df.format(time);
                timeTV.setText(date);
            }else{
                timeTV.setText("");
            }
            CheckBox importantCB = (CheckBox) convertView.findViewById(R.id.importantCB);
            imp = c.isImportant();
            importantCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        c.setImportant(isChecked);
                        cards.set(position,c);
                        mSerializer.saveData(cards);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            importantCB.setChecked(c.isImportant());
            return convertView;
        }
    }
}