package com.exrebound.examrobot;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;


public class FlashCardActivity extends ActionBarActivity{

    ArrayList<Card> cards;

    int pos;
    int total;
    String filename;

    TextView questionTextView, answerTextView, tagTextView, counterTextView;
    CheckBox importantCheckBox;
    ImageButton nextButton, prevButton;

    JSONSerializer tagSerializer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "DEFAULT");
        setContentView(R.layout.activity_flash_card);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ff152a39")));

        filename = getIntent().getStringExtra(CardsListActivity.EXTRA_FILENAME);

        tagSerializer = new JSONSerializer(this, filename);

        //cards = (ArrayList<Card>) getIntent().getSerializableExtra(CardsListActivity.EXTRA_FILENAME);
        try {
            cards = tagSerializer.loadData();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally{
            try {
                tagSerializer.clearFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        total = cards.size();

        Log.d("FlashCardActivity","Cards received: "+total);

        Card c = new Card();
        c.setQuestion(getIntent().getStringExtra(CardsListActivity.EXTRA_QUESTION));
        c.setAnswer(getIntent().getStringExtra(CardsListActivity.EXTRA_ANSWER));
        c.setID(getIntent().getIntExtra(CardsListActivity.EXTRA_ID, 0));
        c.setImportant(getIntent().getBooleanExtra(CardsListActivity.EXTRA_IMPORTANT, false));
        c.setTag(getIntent().getStringExtra(CardsListActivity.EXTRA_TAG));
        c.setTimestamp(getIntent().getStringExtra(CardsListActivity.EXTRA_TIME));

        for(Card card : cards){
            if(card.getID() == c.getID()) {
                pos = cards.indexOf(card);
                break;
            }
        }

        resetViews(c);

        nextButton = (ImageButton) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos < cards.size() - 1) {
                    pos += 1;
                    resetViews(cards.get(pos));
                }
            }
        });
        prevButton = (ImageButton) findViewById(R.id.prevButton);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos > 0) {
                    pos -= 1;
                    resetViews(cards.get(pos));
                }
            }
        });
    }

    private void resetViews(Card c) {
        questionTextView = (TextView) findViewById(R.id.QuestionTV);
        questionTextView.setVisibility(View.VISIBLE);
        questionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });
        answerTextView = (TextView) findViewById(R.id.AnswerTV);
        answerTextView.setVisibility(View.GONE);
        answerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });
        tagTextView = (TextView) findViewById(R.id.TagTV);
        importantCheckBox = (CheckBox) findViewById(R.id.impCB);
        counterTextView = (TextView) findViewById(R.id.counterTV);
        counterTextView.setText((pos+1)+" of "+total);

        questionTextView.setText(c.getQuestion());
        answerTextView.setText(c.getAnswer());
        tagTextView.setText(c.getTag());
        importantCheckBox.setChecked(c.isImportant());
    }

    public void flipCard(){

        View root = findViewById(R.id.root);

        FlipAnimation flip = new FlipAnimation(questionTextView,answerTextView);

        if(questionTextView.getVisibility() == View.GONE){
            flip.reverse();
        }
        root.startAnimation(flip);
    }
}