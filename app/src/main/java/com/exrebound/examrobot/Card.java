package com.exrebound.examrobot;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Zuhaib on 3/21/2015.
 */
public class Card implements Serializable{

    private String question, answer, tag, timestamp;
    private boolean isImportant = false;
    private int id;

    private static final String JSON_ID = "id";
    private static final String JSON_QUESTION = "question";
    private static final String JSON_ANSWER = "answer";
    private static final String JSON_TAG = "tag";
    private static final String JSON_TIMESTAMP = "timestamp";
    private static final String JSON_IMPORTANT = "important";
    private String dateString = "";

    public Card() {
    }

    public Card(JSONObject json) throws JSONException{
        id = json.getInt(JSON_ID);
        question = json.getString(JSON_QUESTION);
        answer = json.getString(JSON_ANSWER);
        tag = json.getString(JSON_TAG);
        timestamp = json.getString(JSON_TIMESTAMP);
        isImportant = json.getBoolean(JSON_IMPORTANT);
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean isImportant) {
        this.isImportant = isImportant;
    }

    //Convert data to JSONObject and return
    public JSONObject toJSON() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(JSON_ID,getID());
        json.put(JSON_QUESTION,getQuestion());
        json.put(JSON_ANSWER,getAnswer());
        json.put(JSON_TAG,getTag());
        json.put(JSON_TIMESTAMP,getTimestamp());
        json.put(JSON_IMPORTANT,isImportant);
        return json;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Card{" +
                "question='" + question + '\'' +
                ", answer='" + answer + '\'' +
                ", tag='" + tag + '\'' +
                ", isImportant=" + isImportant +
                ", id=" + id +
                ", dateString='" + dateString + '\'' +
                '}';
    }

    public Date getDate() {
        Date date = null;
        //Convert date
        if(!getTimestamp().equalsIgnoreCase("null")) {
            long stamp = Long.valueOf(getTimestamp());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(stamp * 1000L);

            Date time = calendar.getTime();

            DateFormat df = new SimpleDateFormat("dd MMM yyyy");
            String dateString = df.format(time);
            try {
                date = df.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }
}
