package com.exrebound.examrobot;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


public class CardFragment extends Fragment {

    public static final String EXTRA_ID = "id";
    public static final String EXTRA_QUESTION = "question";
    public static final String EXTRA_ANSWER = "answer";
    public static final String EXTRA_TAG = "tag";
    public static final String EXTRA_IMPORTANT = "important";

    Card c;

    TextView questionTextView, answerTextView, tagTextView;
    View root;
    CheckBox importantCheckBox;


    public static CardFragment newInstance(int ID,String question, String answer, String tag, boolean isImportant){
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID,ID);
        args.putString(EXTRA_QUESTION, question);
        args.putString(EXTRA_ANSWER, answer);
        args.putString(EXTRA_TAG, tag);
        args.putBoolean(EXTRA_IMPORTANT, isImportant);

        CardFragment fragment = new CardFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(getActivity(), "DEFAULT");
        int id = getArguments().getInt(EXTRA_ID);
        String question = getArguments().getString(EXTRA_QUESTION);
        String answer = getArguments().getString(EXTRA_ANSWER);
        String tag = getArguments().getString(EXTRA_TAG);
        boolean isImportant = getArguments().getBoolean(EXTRA_IMPORTANT);

        Log.d("CardFragment","ID: "+id);
        c = new Card();
        c.setID(id);
        c.setQuestion(question);
        c.setAnswer(answer);
        c.setTag(tag);
        c.setImportant(isImportant);
        //c = ((FlashCardPagerActivity) getActivity()).getCard(id);
    }
    public void flipCard(){

        FlipAnimation flip = new FlipAnimation(questionTextView,answerTextView);

        if(questionTextView.getVisibility() == View.GONE){
            flip.reverse();
        }
        root.startAnimation(flip);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_flash_card, container);
        root = v.findViewById(R.id.root);
        questionTextView = (TextView) v.findViewById(R.id.QuestionTV);
        questionTextView.setVisibility(View.VISIBLE);
        questionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });
        answerTextView = (TextView) v.findViewById(R.id.AnswerTV);
        answerTextView.setVisibility(View.GONE);
        answerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });
        tagTextView = (TextView) v.findViewById(R.id.TagTV);
        importantCheckBox = (CheckBox) v.findViewById(R.id.impCB);


        questionTextView.setText(c.getQuestion());
        answerTextView.setText(c.getAnswer());
        tagTextView.setText(c.getTag());
        importantCheckBox.setChecked(c.isImportant());
        return v;
    }
}
