package com.exrebound.examrobot;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;

/**
 * Created by hp sleek book on 4/28/2015.
 */
public class FontsOverride {
    public static void setDefaultFont(Context context,
                                      String staticTypefaceFieldName) {
        final Typeface regular = Typeface.createFromAsset(context.getAssets(),
                "fonts/calibri.ttf");
        replaceFont(staticTypefaceFieldName, regular);
    }

    protected static void replaceFont(String staticTypefaceFieldName,
                                      final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
