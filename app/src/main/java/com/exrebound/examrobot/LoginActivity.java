package com.exrebound.examrobot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends ActionBarActivity {

    public static final String EXTRA_CALLER = "caller";
    public static final String EXTRA_USERNAME = "username";
    public static final String EXTRA_PASSWORD = "password";

    AutoCompleteTextView usernameTextView;
    EditText passwordEditText;
    Button loginButton;

    String username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "DEFAULT");

        //Get reference to shared prefs
        SharedPreferences prefs = getSharedPreferences("LoginPrefs", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        boolean isInvalidCredentials = false;
        isInvalidCredentials = getIntent().getBooleanExtra(CardsListActivity.EXTRA_INVALID_CREDENTIALS,false);

        //If prefs available but not redirected because of invalid credentials
        if(prefs.getAll().size() > 0 && !isInvalidCredentials){
            Log.d("FlashCardActivity", "Credentials Found!");

            //Extract values from prefs
            username = prefs.getString(EXTRA_USERNAME,null);
            password = prefs.getString(EXTRA_PASSWORD,null);

            if((username != null && !username.isEmpty()) && (password != null && !password.isEmpty())){
                Log.d("FlashCardActivity","Username: "+username+" Password: "+password);

                //Create intent to go from this activity to CardsListActivity
                Intent i = new Intent(LoginActivity.this,FlashCardPagerActivity.class);
                //Intent i = new Intent(LoginActivity.this,CardsListActivity.class);
                //Put values in intent
                i.putExtra(EXTRA_USERNAME, username);
                i.putExtra(EXTRA_PASSWORD, password);
                i.putExtra(EXTRA_CALLER, true);
                //Remove activity from stack
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //Finish activity and start new
                startActivity(i);
                finish();
            }
        }

        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        editor.remove(EXTRA_USERNAME);
        editor.remove(EXTRA_PASSWORD);

        //Ger reference to both text fields
        usernameTextView = (AutoCompleteTextView) findViewById(R.id.username);
        usernameTextView.setTextColor(Color.WHITE);
        passwordEditText = (EditText) findViewById(R.id.password);
        passwordEditText.setTextColor(Color.WHITE);

        //Get reference to login button and set on click listener
        loginButton = (Button) findViewById(R.id.login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Create intent to go from this activity to CardsListActivity
                Intent i = new Intent(LoginActivity.this,FlashCardPagerActivity.class);
                //Intent i = new Intent(LoginActivity.this,CardsListActivity.class);
                //Extract values from text fields
                String cUsername = usernameTextView.getText().toString();
                String cPassword = passwordEditText.getText().toString();
                i.putExtra(EXTRA_CALLER, true);
                //Put values in intent
                i.putExtra(EXTRA_USERNAME, cUsername);
                i.putExtra(EXTRA_PASSWORD, cPassword);
                //If not same username and password
                if(!cUsername.equals(username) && !cPassword.equals(password)) {
                    //Save values to prefs and commit changes
                    editor.putString(EXTRA_USERNAME, cUsername);
                    editor.putString(EXTRA_PASSWORD, cPassword);
                    editor.commit();
                }
                //Remove activity from stack
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //Notify about credentials been saved
                Toast.makeText(LoginActivity.this,"Credentials Saved!",Toast.LENGTH_LONG).show();
                //Finish activity and start new
                startActivity(i);
                finish();
            }
        });
    }
}