package com.exrebound.examrobot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FlashCardPagerActivity extends ActionBarActivity{

    public static final String EXTRA_USERNAME = "username";
    public static final String EXTRA_PASSWORD = "password";
    public static final String EXTRA_CALLER = "caller";

    SharedPreferences prefs;

    ArrayList<MenuItem> categoriesList;
    MenuItemAdapter MenuItemAdapter;
    ListView drawerListView;

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle mDrawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "DEFAULT");
        setContentView(R.layout.activity_flash_card_host);

        String username = getIntent().getStringExtra(LoginActivity.EXTRA_USERNAME);
        String password = getIntent().getStringExtra(LoginActivity.EXTRA_PASSWORD);
        boolean isCalledFromParent = getIntent().getBooleanExtra(LoginActivity.EXTRA_CALLER,false);

        SetUp();

        prefs = getSharedPreferences("CounterPrefs", Context.MODE_PRIVATE);

        Bundle args = new Bundle();
        args.putString(EXTRA_USERNAME, username);
        args.putString(EXTRA_PASSWORD, password);
        args.putBoolean(EXTRA_CALLER,isCalledFromParent);

        Fragment detail = new CardsListActivity();
        detail.setArguments(args);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.ContentFrame, detail,"Frag").commit();

    }


    public void SetUp() {
        drawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        drawerListView = (ListView) findViewById(R.id.list);
        drawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drawerLayout.closeDrawers();
                MenuItem i = (MenuItem) drawerListView.getItemAtPosition(position);
                String c = i.getItemName();

                CardsListActivity fragment = (CardsListActivity) getSupportFragmentManager().findFragmentByTag("Frag");

                if(c.equalsIgnoreCase("Synchronize")){
                    fragment.refresh();
                }else if(c.equalsIgnoreCase("Filter By Tags")){
                    fragment.FilterByTag();
                }else if(c.equalsIgnoreCase("Filter By Date")){
                    fragment.FilterByDateWithDialog();
                }else if(c.equalsIgnoreCase("Important Cards")){
                    fragment.FilterByImportant();
                }else if(c.equalsIgnoreCase("Exit")){
                    exit();
                }
            }
        });
        categoriesList = new ArrayList<>();
        categoriesList = getCategoriesList();
        MenuItemAdapter = new MenuItemAdapter(this,R.layout.menu_item,categoriesList);
        drawerListView.setAdapter(MenuItemAdapter);

        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.DrawerOpen, R.string.DrawerClose) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle(Category);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //getSupportActionBar().setTitle(Category);

            }
        };
        drawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<MenuItem> getCategoriesList() {
        categoriesList = new ArrayList<>();

        categoriesList.add(new MenuItem("Synchronize",R.drawable.update));
        categoriesList.add(new MenuItem("Filter By Tags", R.drawable.tag));
        categoriesList.add(new MenuItem("Filter By Date",R.drawable.today));
        categoriesList.add(new MenuItem("Important Cards",R.drawable.star));
        categoriesList.add(new MenuItem("Exit",R.drawable.close));
        return categoriesList;
    }

    private void exit(){
        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private class MenuItemAdapter extends ArrayAdapter<MenuItem>{

        public MenuItemAdapter(Context context, int resource, List<MenuItem> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getLayoutInflater().inflate(R.layout.menu_item,null);

            MenuItem item = getItem(position);

            TextView ItemTitleTV = (TextView) convertView.findViewById(R.id.ItemTitleTV);
            ItemTitleTV.setText(item.getItemName());

            ImageView IconIV = (ImageView) convertView.findViewById(R.id.IconIV);
            IconIV.setImageResource(item.getDrawableID());

            return convertView;
        }
    }
}
