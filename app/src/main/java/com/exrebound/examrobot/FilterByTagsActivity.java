package com.exrebound.examrobot;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;


public class FilterByTagsActivity extends ActionBarActivity {

    protected static final String EXTRA_TAG = "tag";

    ListView cardsList;
    ArrayList<String> tags;

    TagAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "DEFAULT");
        setContentView(R.layout.activity_filter_by_tags);

        tags = getIntent().getStringArrayListExtra(CardsListActivity.EXTRA_TAG_ARRAY);

//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(R.color.colorPrimary));

        cardsList = (ListView) findViewById(R.id.cards_listTags);
        cardsList.setEmptyView(findViewById(R.id.emptyViewTags));
        adapter = new TagAdapter(this,R.layout.card_tag_item,tags);
        cardsList.setAdapter(adapter);
        cardsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String tag = (String) cardsList.getItemAtPosition(position);
                Intent i = new Intent(FilterByTagsActivity.this,CardsByTagActivity.class);
                i.putExtra(EXTRA_TAG, tag);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_filter_by, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.MIHome) {
            startActivity(new Intent(this,CardsListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //Adapter for Tags//
    public class TagAdapter extends ArrayAdapter<String>{
        public TagAdapter(Context context, int resource, ArrayList<String> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getLayoutInflater().inflate(R.layout.card_tag_item, null);

            String title = getItem(position);
            TextView titleTV = (TextView) convertView.findViewById(R.id.tagTitleTV);
            titleTV.setText(title);
            return convertView;
        }
    }
}
