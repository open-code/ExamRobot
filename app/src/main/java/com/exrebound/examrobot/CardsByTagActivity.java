package com.exrebound.examrobot;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class CardsByTagActivity extends ActionBarActivity {

    ListView cardsList;
    ArrayList<Card> cardsByTags;
    boolean imp;
    JSONSerializer serializer, tagSerializer;
    ArrayList<Card> cards;
    CardsListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "DEFAULT");
        setContentView(R.layout.activity_cards_by_tag);

//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(R.color.colorPrimary));

        //Create a local ArrayList
        cardsByTags = new ArrayList<>();

        //Get tag
        String tag = getIntent().getStringExtra(FilterByTagsActivity.EXTRA_TAG);

        try {
            //Load all Cards
            serializer = new JSONSerializer(this,"Cards.json");
            tagSerializer = new JSONSerializer(this,"Tags.json");
            //Store locally
            cards = serializer.loadData();
            //Check for tags
            for(Card c : cards){
                //If matches
                if(c.getTag().equalsIgnoreCase(tag)){
                    //Add to local ArrayList
                    cardsByTags.add(c);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("CardsByTagActivity",cardsByTags.size()+" cards in list");

        //Get reference to ListView
        cardsList = (ListView) findViewById(R.id.cards_listByTags);
        //Set empty view
        cardsList.setEmptyView(findViewById(R.id.emptyViewByTags));
        //Create adapter
        adapter = new CardsListAdapter(this, R.layout.card_item, cardsByTags);
        //Set adapter
        cardsList.setAdapter(adapter);
        //Set item click listener
        cardsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Get card at pos
                Card c = (Card) cardsList.getItemAtPosition(position);
                Log.d("CardsByTagActivity",c.toString()+"\n");

                //Save cards to file
                try {
                    tagSerializer.saveData(cardsByTags);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //Create intent
                Intent i = new Intent(CardsByTagActivity.this, FlashCardActivity.class);
                //Put all values of current card
                i.putExtra(CardsListActivity.EXTRA_FILENAME,"Tags.json");
                i.putExtra(CardsListActivity.EXTRA_QUESTION, c.getQuestion());
                i.putExtra(CardsListActivity.EXTRA_ANSWER, c.getAnswer());
                i.putExtra(CardsListActivity.EXTRA_TAG, c.getTag());
                i.putExtra(CardsListActivity.EXTRA_TIME, c.getTimestamp());
                i.putExtra(CardsListActivity.EXTRA_IMPORTANT, c.isImportant());
                i.putExtra(CardsListActivity.EXTRA_ID,c.getID());
                i.putExtra(CardsListActivity.EXTRA_POSITION, position);
                //Start activity
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_filter_by, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.MIHome) {
            startActivity(new Intent(this,CardsListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //Adapter for Cards//
    private class CardsListAdapter extends ArrayAdapter<Card>{
        public CardsListAdapter(Context context, int resource, ArrayList<Card> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getLayoutInflater().inflate(R.layout.card_item, null);

            //Get card on the view
            final Card c = getItem(position);
            //Get reference to views and set values according to card
            TextView titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            titleTV.setText(c.getQuestion());
            TextView answerTV = (TextView) convertView.findViewById(R.id.answerTV);
            answerTV.setText(c.getAnswer());
            TextView tagTV = (TextView) convertView.findViewById(R.id.tagTV);
            tagTV.setText(c.getTag());
            TextView timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            if(!c.getTimestamp().equals("null")) {
                //Convert date
                long stamp = Long.valueOf(c.getTimestamp());
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(stamp * 1000L);

                Date time = calendar.getTime();

                DateFormat df = new SimpleDateFormat("dd MMM yyyy");
                String date = df.format(time);
                timeTV.setText(date);
            }else{
                timeTV.setText("");
            }
            CheckBox importantCB = (CheckBox) convertView.findViewById(R.id.importantCB);
            imp = c.isImportant();
            //Set on check change listener
            importantCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        //Set card's value to same as the current state of CheckBox
                        c.setImportant(isChecked);
                        //update value of card in both lists
                        cardsByTags.set(position,c);
                        cards.set(cards.indexOf(c),c);
                        //Save date with respect ot all cards
                        serializer.saveData(cards);
                        //Notify
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            //Notify checkbox as well
            importantCB.setChecked(c.isImportant());
            return convertView;
        }
    }

}
