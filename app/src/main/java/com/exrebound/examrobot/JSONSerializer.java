package com.exrebound.examrobot;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by Zuhaib on 3/25/2015.
 */
public class JSONSerializer {
    private Context mContext;
    private String mFileName;

    public JSONSerializer(Context mContext, String mFileName) {
        this.mContext = mContext;
        this.mFileName = mFileName;
    }

    public void saveData(ArrayList<Card> cards)throws JSONException,IOException{
        //Build JSON array
        JSONArray array = new JSONArray();
        for(Card c : cards)
            array.put(c.toJSON());


        //Write file to disk
        Writer writer = null;
        try{
            //Create a Output stream towards a File
            OutputStream out = mContext.openFileOutput(mFileName,Context.MODE_PRIVATE);
            //Create a writer to write data into that stream
            writer = new OutputStreamWriter(out);
            //finally write String formatted array to the file using writer
            writer.write(array.toString());
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            //If writer available, close it
            if(writer != null)
                writer.close();
        }
    }

    public ArrayList<Card> loadData() throws IOException,JSONException {
        ArrayList<Card> cards = new ArrayList<>();
        BufferedReader reader = null;
        try{
            //Open and read the file into buffered reader
            InputStream in = mContext.openFileInput(mFileName);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null)
                jsonString.append(line);
            //Parse the JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
            //Build array of cards from JSONObjects
            for(int i = 0 ; i < array.length() ; i++)
                cards.add(new Card(array.getJSONObject(i)));
        }catch(FileNotFoundException e){
            //Ignore
        }finally{
            if(reader != null)
                reader.close();
        }
        return cards;
    }

    public void clearFile() throws IOException {
        if(!mFileName.equalsIgnoreCase("Cards.json")) {
            FileOutputStream writer = mContext.openFileOutput(mFileName, Context.MODE_PRIVATE);
            writer.write((new String()).getBytes());
            writer.close();
        }

        File f = new File(mFileName);
        if(f.length() <= 1)
            Log.d("Serializer","File Cleared!");
        else
            Log.d("Serializer","File still has contents!");
    }
}
